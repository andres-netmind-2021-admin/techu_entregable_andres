package com.techu.entregables.modelo;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

public class ModeloProductoTest {

    ModeloProducto p = new ModeloProducto(1, "Adidas", "Zapatillas", 189.00);

    @BeforeEach
    void aPriori() {
        System.out.println("=========  @BeforeEach");
         p = new ModeloProducto(1, "Adidas", "Zapatillas", 189.00);
    }

    @AfterEach
    void aPosteriori() {
        System.out.println("=========  @AfterEach");
    }

    @BeforeAll
    static void antesDeTodos() {
        System.out.println("=========  @BeforeAll");
    }

    @AfterAll
    static void despuesDeTodos() {
        System.out.println("=========  @AfterAll");
    }

    @Test
    void testPrecio() {
        System.out.println("=========  @Test testPrecio()");
        assertEquals(189.00, p.getPrecio());
        p.setPrecio(200.00);
        assertEquals(200.00, p.getPrecio());
    }

    @Test
    void testConstructor() {
        System.out.println("=========  @Test testConstructor()");
        assertEquals(1, p.getId());
        assertEquals("Adidas", p.getMarca());
        assertEquals("Zapatillas", p.getDescripcion());
        assertEquals(189.00, p.getPrecio());
        //assertTrue(p.getUsuarios().isEmpty());
    }

    @Test
    void testListaUsuariosNuncaVacia() {
        System.out.println("=========  @Test testListaUsuariosNuncaVacia()");
        assertNotNull(p.getUsuarios());
    }

    @Test
    void testListaNoVaciaUsuario() {
        System.out.println("=========  @Test testListaNoVaciaUsuario()");
        assertTrue(p.getUsuarios().isEmpty());
        p.getUsuarios().add(new ModeloUsuario(1, "Pepito"));
        assertFalse(p.getUsuarios().isEmpty());
        assertEquals(1, p.getUsuarios().size());
        assertEquals(1, p.getUsuarios().get(0).getId());
        assertEquals("Pepito", p.getUsuarios().get(0).getNombre());
    }

}
